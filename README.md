## Atividade 3
dupla
dados do censo Ensino Superior 2009-2020
(Evasão)

1- ETL (extract, transform and load) em ".ipynb
2- Relatório de apresentação dos resultados
    - latex
    - streamlit
    - Data studio, power bi, tableau, qlick sense...
    - Markdown/Jupyter NB

# Questões norteadoras
1- contexto?
2- público-alvo?
3- ação esperada?
4- informação x narrativa
    5- visual adequado
    6- sem saturação de cores

# Site recomendado
storytellingwithdata.com

* Rascunho
* Design
* Foco da análise


# TEMA
* Evasão do Ensino Superior
(precisa de um enfoque: por cor, por renda, pré pandemia, por tipo de entrada(cotas), por tipo de universidade)
* Público-Alvo: gestores/formuladores (nacional, por universidade) de políticas do Ensino Superior (ES)
* Nível: universidade? curso? área?

# PROX AULA 23/11
1. público-alvo
2. enfoque
3. ação esperada

# Data/Looke studio (Google)
* Visualização de dados (online)
    * datastudio.google.com
    * adição de dados
    * criar o relatório
    * distribuir o relatório
* Conhecimento
* Informação
* Dados
    * MS -> datasus
    * MEC -> inep
    * MCidadania -> SAGI
    * MME -> ANP, ANEEL

# Business problema
* dados
* insights
* preparação de dados
* modelo
* avaliação do modelo
* visualização
* deploy (streamlit)

## trabalho
* ETL em .ipynb
# Código-fonte
    * descritivo
    * prints
# Relatório
    * Links (etl e dashboard) + arquivo descrito
# 3º arquivo
* Explicar como o dashboard ajudará a tomada de decisão?
    * print dos gráficos (de linha, por ex) e explicação do gráfico
## Apresentação
* capa
* estrutura de apresentação
    * cabeçalho e rodapé
* fonte de dados
* período de análise
* apresentação do dashboard

## Venv
pip install virtualenv
python -m venv venv
source venv/Scripts/activate

## Instalação de Pacotes
pip install --upgrade pip
pip freeze > requirements.txt
pip install -r requirements.txt